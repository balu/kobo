#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
#include <include/linux/input.h>

struct input_event ie;
/*
struct input_event {
      struct timeval time; 
      unsigned short type; // 2
      unsigned short code; // 2
      unsigned int value;  // 4
      };
*/

int main(int argc, char* argv[])
{

   FILE* fevent = fopen("/dev/input/event1", "rb");
   if(fevent == NULL){
      printf("Need /dev/input/event1 to read events\n");
      exit(-1);
   }
   while(1){
      fread(&ie, 16, 1, fevent);
      if(ie.type == EV_SYN){
         continue;
      } else if( ie.type == EV_KEY){
         if( ie.code == BTN_TOUCH) // Finger event 
         {
            if(ie.value == 1){ // gedrueckt
               printf("KEY PRESS\n");
            } else if (ie.value == 0){ // geloest
               printf("KEY RELEASE\n");
            }
         } else {
             printf("KEY %x %d\n",ie.code,ie.value);
         }
      } else if( ie.type == EV_ABS){
         if(ie.code == ABS_X) { //X POS
            printf("ABS X %d\n",ie.value);
         } else if (ie.code == ABS_Y) { //Y POS
            printf("ABS Y %d\n",ie.value);
         } else if (ie.code == ABS_PRESSURE) { //druck 
            printf("ABS PRESSURE %d\n",ie.value);
         } else {
            printf("ABS %x %d\n",ie.code,ie.value);
         }
      }
   }

   fclose(fevent);
   return 0;
}
