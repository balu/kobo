#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "font24.h"

#define BORDER 8
#define ADDTOP 6

unsigned char img[800][800];
unsigned char img565[600][800][2];

char stringBuffer[4096];
int width, height;

typedef struct
{
   int x, y; 
} Vec2d;

void init()
{
   memset(img, 255, 800 * 800);
}

void imgLine(int x, int y, int xx, int yy)
{
   int x0 = (x), y0 = (y), x1 = (xx), y1 = (yy);
   int dx =  abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
   int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
   int err = dx + dy, e2;

   for(;;)
   {
      if(x0 >= 0 && x0 < width && y0 >= 0 && y0 <height) img[y0][x0] = 0;
      if (x0 == x1 && y0 == y1) break;
      e2 = 2 * err;
      if (e2 >= dy) {err += dy; x0 += sx;}
      if (e2 <= dx) {err += dx; y0 += sy;}
   }
}

void imgBox(int x, int y, int xx, int yy){
   imgLine(x,y,xx,y);
   imgLine(x,y,x,yy);
   imgLine(xx,y,xx,yy);
   imgLine(x,yy,xx,yy);
}

Vec2d drawString(char* str, int x, int y, int box)
{
   const char *p;
   int x0 = x;
   int y0 = y;
   int x1 = 0;
   int y1 = 0;
   Vec2d bound = {x, y + FONT_HEIGHT};

   for (p = str; *p; ++p)
   {
      if(*p == '\n')
      {
         x = x0;
         y += FONT_HEIGHT;
         bound.y = y + FONT_HEIGHT;
         continue;
      }
      if(*p == '\t')
      {
         x += 3 * FONT_STRIDE;
         continue;
      }
      if(*p < 32 || *p > 127)
      {
         continue;
      }
      
      int xmin, xmax, ymin, ymax;
      xmin = x;
      ymin = y;
      xmax = x + FONT_WIDTH - 1;
      ymax = y + FONT_HEIGHT - 1;

      if(xmin >= 0 && ymin >= 0 && xmax < (width - BORDER) && ymax < (height - BORDER))
      {
         for(int yl = 0; yl < FONT_HEIGHT; yl++)
         {
            for(int xl = 0; xl < FONT_WIDTH; xl++)
            {
               int xll, yll;
               xll = x + xl;
               yll = y + yl;
               
               int v1 = 255 - img[yll][xll];
               int v2 = 255 - font[yl][(*p - 32) * 16 + xl];
               int target = 255 - (v1 + v2);
               if(target < 0) target = 0;
               img[yll][xll] = target;
               if(xll > x1) x1 = xll;
               if(yll > y1) y1 = yll;
            }
         }
      }
      x += FONT_STRIDE;
   }

   if(x > bound.x) bound.x = x; 
   
   if(box)
   {
      imgBox(x0 - 3, y0 - 3, x1, y1);
      imgBox(x0 - 4, y0 - 4, x1 + 1, y1 + 1);
   }

   return bound;
}

void create565(int landscape)
{
   for(int y = 0; y < 600; y++)
   {
      for(int x = 0; x < 800; x++)
      {
         int xl = landscape ? x : 599 - y;
         int yl = landscape ? y : x;
         img565[y][x][0] = 0;
         img565[y][x][1] = 0;
         img565[y][x][1] |= (img[yl][xl] / 8) << 3;
         img565[y][x][1] |= (img[yl][xl] / 4) >> 3;
         img565[y][x][0] |= (img[yl][xl] / 4) << 5;
         img565[y][x][0] |= (img[yl][xl] / 8);
      }
   }
}

int main(int argc, char* argv[])
{
   int landscape = 0;
   int output = 0;
   int box = 0;

   init();
   
   for(int i = 1; i < argc; i++)
   {
      if (strcmp(argv[i], "-l") == 0)
      {
         landscape = 1;
      }
      else if (strcmp(argv[i], "-o") == 0)
      {
         output = 1;
      }
      else if (strcmp(argv[i], "-b") == 0)
      {
         box = 1;
      }
      else
      {
         fprintf(stderr, "\n%s reads up to 4096 characters from stdin and renders the text on the kobo\n\nusage: %s [-l][-o]\n\noptions:\n   -l   landscape mode\n   -o   output to stdout instead of e-ink\n   -b   draw box around text\n\n", argv[0], argv[0]);
         exit(-1);
      }
   }
   
   width = landscape ? 800 : 600;
   height = landscape ? 600 : 800;
   
   fread(stringBuffer, 1, 4096, stdin);
   fclose(stdin);
   
   drawString(stringBuffer, BORDER, BORDER + ADDTOP, box);

   create565(landscape);

   if(output)
   {
      fwrite(img565, 2, 800 * 600, stdout);
   }
   else
   {
      FILE* pickel = popen("/usr/local/Kobo/pickel showpic", "w");
      fwrite(img565, 2, 800 * 600, pickel);
      pclose(pickel);
   }
   
   return 0;
}
